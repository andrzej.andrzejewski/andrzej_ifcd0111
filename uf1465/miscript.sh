#!/bin/bash
# Bohdan Grebecki 2018
# Curso programacion estructurada IFCD0111

##Tarea programada en crontab##
#* * * * * /bin/bash /home/henio/andrzej_ifcd0111/uf1465/miscript.sh > /var/www/html/info.html


# Monitorizar servidor
echo
echo "Bienvenido Bohdan, esta es la informacion del servidor:"
echo "-------------------------------------------------------"
echo "Usuario"
whoami
echo "-------------------------------------------------------"
echo "Dia"
date
echo "-------------------------------------------------------"
echo "Version del SO"
uname -a
echo "-------------------------------------------------------"
echo "Esta es la ocupacion de disco y su memoria RAM"
echo
df -h
echo "-------------------------------------------------------"
free -h
echo "-------------------------------------------------------"
echo "Estos son los datos del Task Manager. Presiona "q" para parar y pasar a otro comando."
echo
top -n 1
echo "-------------------------------------------------------"
echo "Estos son los datos de la CPU"
echo
cat /proc/cpuinfo